import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
// import {FormGroup, FormControl, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // private formBuilder: FormBuilder
  constructor(private authService: AuthService) {
  }

  ngOnInit() {
  }

  login() {
    const object = {
      email: 'e.trudini@cgmconsulting.it',
      password: 'Test123@'
    };

    this.authService.login(object).subscribe((response) => {
      console.log(response);
      localStorage.setItem('cgm_token', JSON.stringify(response['token']));
      localStorage.setItem('cgm_user', JSON.stringify(response['user']));
      // localStorage.setItem('cgm_surname', JSON.stringify(response.user.surname));
    }, (err) => console.log('error:', err));


  }


}
