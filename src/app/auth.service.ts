import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) {
  }

  private HttpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};


  login(bodyObject: any) {
    return this.httpClient.post('http://192.168.1.189:3000/auth/login', bodyObject, {headers: this.HttpOptions.headers});
  }
}
